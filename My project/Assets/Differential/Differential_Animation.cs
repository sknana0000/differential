using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Differential_Animation : MonoBehaviour
{
    public GameObject VorGear;
    public GameObject MiddleGear;

    public GameObject EpicyclicGear01;
    public GameObject EpicyclicGear02;
    public GameObject RightGear;
    public GameObject LeftGear;
    public GameObject Middle_01;
    public GameObject Middle_02;

    private float Speedmiddle = 0.33333f;

    private float MiddleGearSpeed;


    public float Speed = 1.0f;

    private float SpeedRight;
    private float SpeedLeft;


    //public float SpeedControl=0f;

    // private int SpeedLeftsprren = 0;
    // public float  SpeedLeftMaxValue = 2f;
    // private float SpeedLeftSpeed;


    // private float SpeedEpicyclicGearSperren = 0.0f;
    //  public float  SpeedEpicyclicGearValue;
    // private float SpeedEpicyclicGearSpeed;




    [Range(0, 1)] public float SpeedProzess = 0f;
   // [Range(0, 1)] public float SpeedLeftProzess = 1;

    



    //public float SpeedEpicyclicGear02 = 0.0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        VorGear.transform.Rotate(-Speed * Time.deltaTime, 0, 0);

        MiddleGearSpeed = Speed * Time.deltaTime * Speedmiddle;


        MiddleGear.transform.Rotate(0, MiddleGearSpeed, 0);


        SpeedRight = Mathf.Lerp(0f, MiddleGearSpeed, SpeedProzess);
        SpeedLeft = Mathf.Lerp(MiddleGearSpeed, 0f, SpeedProzess);

       // SpeedEpicyclicGearSpeed = Mathf.Lerp(SpeedEpicyclicGearSperren, SpeedEpicyclicGearValue, SpeedLeftProzess);

        



        RightGear.transform.Rotate(0, SpeedRight*2, 0);
        LeftGear.transform.Rotate(0, SpeedLeft*2, 0);



      //  Middle_01.transform.Rotate(Speed * Time.deltaTime * Speedmiddle * SpeedEpicyclicGearSpeed, 0, 0);
      // Middle_02.transform.Rotate(-Speed * Time.deltaTime * Speedmiddle * SpeedEpicyclicGearSpeed, 0, 0);





    }
}
