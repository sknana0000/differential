﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class DrageshowPicture : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{


    //开始位置
    public Vector3 startPosition;
    //位置差,用来保证和鼠标的位置差
    Vector3 deltaPosiiton;
    //需要填充现实的图片
    public Image BGimage;
    //开始拖拽
    public void OnBeginDrag(PointerEventData eventData)
    {
        startPosition = eventData.position;
        deltaPosiiton = transform.position - (Vector3)eventData.position;
    }
    //拖拽中


    public void OnDrag(PointerEventData eventData)
    {
        Vector3 change = (Vector3)eventData.position + deltaPosiiton;
        Vector3 v3 = Vector3.zero;
        v3.x = change.x;
        v3.y = transform.position.y;
        v3.z = transform.position.z;
        //求得填充图片的宽度之后,求得边缘再屏幕上的位置,用来固定拖拽按钮不超过
        float xmin = BGimage.transform.position.x - BGimage.GetComponent<RectTransform>().sizeDelta.x / 2;
        float xmax = BGimage.transform.position.x + BGimage.GetComponent<RectTransform>().sizeDelta.x / 2;
        Debug.Log(xmin);
        if (v3.x < xmin)
        {
            v3.x = xmin;
        }
        else if (v3.x > xmax)
        {
            v3.x = xmax;
        }



        transform.position = v3;
        //根据按钮盒屏幕边缘的距离,求得百分比,给填充值
        BGimage.fillAmount = (xmax - v3.x) / (xmax - xmin);
    }
    //结束拖拽
    public void OnEndDrag(PointerEventData eventData)
    {
    }


}