using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnGrid : MonoBehaviour
{
	
	public GameObject[] prefabToSpawn;
	
	public int gridX;
	public int gridZ;
	public float gridSpcingOffset = 1f;
	public Vector3 gridOrigin = Vector3.zero;
	public float randomZ = 0f;
	
    // Start is called before the first frame update
    void Start()
    {
		SpawnGrid();
	}
	
	void SpawnGrid()
	{
		for (int x = 0; x < gridX; x++)
		{
           for (int z = 0; z < gridZ; z++)
		   {
			    Vector3 spawnPosition = new Vector3(x * gridSpcingOffset, 0, z * gridSpcingOffset) + gridOrigin;
				PickAndSpawn(spawnPosition, Quaternion.Euler(new Vector3( Random.Range(0, 360), Random.Range(0, 360),  Random.Range(0, 360))));
		   }
		}
	}
		
    void PickAndSpawn(Vector3 positionToSpawn, Quaternion rotationToSpawn)
	{
         int randomIndex = Random.Range (0, prefabToSpawn.Length);
		 GameObject clone = Instantiate(prefabToSpawn[randomIndex], positionToSpawn, rotationToSpawn); 
	}

    void Update()
	{
        
       
	}

}










