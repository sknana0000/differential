using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Speedo11 : MonoBehaviour
{
    [SerializeField]
    private Image imageNeedle;
    [SerializeField]
    private Slider sliderSpeed;

    private float currentSpeed = 0;
    private float targetSpeed = 0;
    private float needleSpeed = 130.0f;

    //Start is called before the first frame update
    void Start()
    {
        
    }
   

    // Update is called once per frame
    void Update()
    {
        Debug.Log(+ sliderSpeed.value);
        Debug.Log(+ currentSpeed);

        if(targetSpeed != currentSpeed)
        {  
            UpdateSpeed();
        }     
    }
    
    public void SetSpeedFromSlider()
    {
        targetSpeed = sliderSpeed.value;
    }
    public void SetSpeed(float amt)
    {
        targetSpeed = amt;
    }


    void UpdateSpeed()
    {
        if(targetSpeed > currentSpeed)
        {
          currentSpeed += Time.deltaTime * needleSpeed;
          currentSpeed = Mathf.Clamp(currentSpeed, 0.0f, targetSpeed);
        }
        else if (targetSpeed < currentSpeed)
       {
          currentSpeed -= Time.deltaTime * needleSpeed;
          currentSpeed = Mathf.Clamp(currentSpeed, 0.0f, 260.0f);
        }
        
        SetNeeld();
    
    }
    void SetNeeld()
    {
        imageNeedle.transform.localEulerAngles = new Vector3(0,0,(currentSpeed / 260.0f * 240.0f - 95.0f) * -1.0f);
    }
   




}
